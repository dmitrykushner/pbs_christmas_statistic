//
//  Result.swift
//  Models
//
//  Created by Igor Rapinchuk on 8.06.21.
//

import Foundation

public typealias EmptyResult = (Result<Void>) -> Void

public enum Result<T> {
    case value(T)
    case error(Error)
}
