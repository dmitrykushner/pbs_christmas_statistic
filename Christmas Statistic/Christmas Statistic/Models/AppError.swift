//
//  AppError.swift
//  UseCases
//
//  Created by Igor Rapinchuk on 27.05.21.
//

import Foundation

public enum AppError: Error, Equatable {
    case empty

    public var message: String? {
        return nil
    }

    var localizedDescription: String? {
        message
    }

    public var code: Int? {
        nil
    }

}
