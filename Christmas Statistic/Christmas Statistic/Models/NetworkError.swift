//
//  NetworkError.swift
//  UseCases
//
//  Created by Igor Rapinchuk on 27.05.21.
//

import Foundation

// MARK: - NetworkError

public class NetworkError: Codable, Error {
    enum CodingKeys: String, CodingKey {
        case message
    }

    public let message: String
    public var code: Int = 0

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        message = try container.decode(String.self, forKey: .message)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(message, forKey: .message)
    }
}
