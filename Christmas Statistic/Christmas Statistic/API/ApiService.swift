//
//  ApiService.swift
//  Christmas Statistic
//
//  Created by Dmitry Kushner on 25.11.21.
//

import Foundation
import Moya

protocol ApiService {
    func getUserStatistic(completion: @escaping Completion<[String: TelegramUserDTO]>)
}

final class ApiServiceImpl: ApiService {
    private let request = RequestManager()
    
    func getUserStatistic(completion: @escaping Completion<[String: TelegramUserDTO]>) {
        request.exec(
            baseURL: URL(string: "https://pbs-bot.hackcat.ml/")!,
            path: "users.json",
            method: .get,
            completion: completion
        )
    }
}

// MARK: - BaseRequestService

enum RequestMethod {
    case get
    case post
    case put
    case delete
    case patch
}

typealias Completion<ResponseType> = (Result<ResponseType>) -> Void

// MARK: - RequestManager

class RequestManager {
    //  swiftlint:disable all
    struct BaseTargetType: TargetType {
        var baseURL: URL
        var path: String
        var method: Moya.Method
        var sampleData = Data()
        var task: Task
        var headers: [String: String]?
    }

    func exec<ResponseType: Decodable>(
        baseURL: URL,
        path: String,
        parameters: [String: Any]? = nil,
        body: [String: Any]? = [:],
        headers: [String: String]? = nil,
        method: RequestMethod,
        completion: @escaping Completion<ResponseType>
    ) {
        var task = Task.requestPlain
        if let parameters = parameters, !parameters.isEmpty {
            task = Task.requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
        if let body = body, !body.isEmpty,
            let theJSONData = try? JSONSerialization.data(withJSONObject: body, options: [])
        {
            task = Task.requestCompositeData(bodyData: theJSONData, urlParameters: parameters ?? [:])
        }

        let target = BaseTargetType(
            baseURL: baseURL,
            path: path,
            method: moyaMethod(method: method),
            task: task,
            headers: [:]
        )
        let endpointClosure: MoyaProvider<BaseTargetType>.EndpointClosure = { _ in
            self.endpoint(target: target)
        }

        MoyaProvider(endpointClosure: endpointClosure).request(target) { result in
            DispatchQueue.global(qos: .userInitiated).async {
                switch result {
                case let .success(response):

                    guard !response.data.isEmpty else {
                        return
                    }

                    do {
                        do {
                            try JSONSerialization.jsonObject(with: response.data, options: [])
                        } catch {

                        }
                        print(response.data.prettyPrintedJSONString)
                        let string = String(decoding: response.data, as: UTF8.self)
                        if string.isEmpty,
                            let model = try? JSONDecoder().decode(ResponseType.self, from: Data("{}".utf8))
                        {
                            DispatchQueue.main.async {
                                completion(.value(model))
                            }
                            return
                        }

                        let responseModel = try response.map(ResponseType.self)

                        DispatchQueue.main.async {
                            completion(.value(responseModel))
                        }
                    } catch {
                        print(error)
                        do {
                            guard let error = try? response.map(NetworkError.self) else {
                                DispatchQueue.main.async {
                                    completion(.error(error))
                                }
                                return
                            }
                            error.code = response.statusCode
                            DispatchQueue.main.async {
                                completion(.error(error))
                            }
                        } catch {
                            guard !response.data.isEmpty else {
                                DispatchQueue.main.async {
                                    completion(.error(AppError.empty))
                                }
                                return
                            }
                            DispatchQueue.main.async {
                                completion(.error(error))
                            }
                        }
                    }

                case let .failure(error):
                    DispatchQueue.main.async {
                        completion(.error(error))
                    }
                }
            }
        }
    }

   

    // swiftlint:enable all

    private func endpoint(target: BaseTargetType) -> Endpoint {
        Endpoint(
            url: URL(target: target).absoluteString.removingPercentEncoding ?? "",
            sampleResponseClosure: { .networkResponse(200, target.sampleData) },
            method: target.method,
            task: target.task,
            httpHeaderFields: target.headers
        )
    }

    private func moyaMethod(method: RequestMethod) -> Moya.Method {
        switch method {
        case .get:
            return Moya.Method.get

        case .post:
            return Moya.Method.post

        case .put:
            return Moya.Method.put

        case .delete:
            return Moya.Method.delete

        case .patch:
            return Moya.Method.patch
        }
    }
}

extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
            let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
            let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}
