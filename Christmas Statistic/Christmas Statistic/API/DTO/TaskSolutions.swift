//
//  TaskSolutions.swift
//  Christmas Statistic
//
//  Created by Dmitry Kushner on 3.01.22.
//

import Foundation

struct TaskSolutions: Codable {
    let one: [String]??
    let two: [String]?
    let three: [String]?
    let four: [String]?
    let five: [String]?
    let six: [String]?
    let seven: [String]?
    let eight: [String]?
    let nine: [String]?
    let ten: [String]?
    let eleven: [String]?
    let twelve: [String]?
    let thirteen: [String]?
    let fourteen: [String]?
    let fifteen: [String]?
    let sixteen: [String]?
    let seventeen: [String]?
    let eighteen: [String]?
    let nineteen: [String]?
    let twenty: [String]?
    let twentyOne: [String]?
    let twentyTwo: [String]?
    let twentyThree: [String]?
    let twentyFour: [String]?
    let twentyFive: [String]?
    
    enum CodingKeys: String, CodingKey {
        case one = "1"
        case two = "2"
        case three = "3"
        case four = "4"
        case five = "5"
        case six = "6"
        case seven = "7"
        case eight = "8"
        case nine = "9"
        case ten = "10"
        case eleven = "11"
        case twelve = "12"
        case thirteen = "13"
        case fourteen = "14"
        case fifteen = "15"
        case sixteen = "16"
        case seventeen = "17"
        case eighteen = "18"
        case nineteen = "19"
        case twenty = "20"
        case twentyOne = "21"
        case twentyTwo = "22"
        case twentyThree = "23"
        case twentyFour = "24"
        case twentyFive = "25"
    }
}

extension Encodable {
    public func toDictionary() -> [String: AnyObject]? {
        do {
            let data = try JSONEncoder().encode(self)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            return json as? [String: AnyObject]
        } catch {
            return nil
        }
    }
}
