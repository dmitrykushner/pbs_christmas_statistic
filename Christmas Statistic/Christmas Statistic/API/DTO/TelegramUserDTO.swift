//
//  TelegramUserDTO.swift
//  Christmas Statistic
//
//  Created by Dmitry Kushner on 25.11.21.
//

import Foundation

struct TelegramUserDTO: Decodable {
    /// Id телеграмм пользователя
    let uid: UInt
    /// Созданные пользователем профили
    let profiles: [ProfileDTO]
    /// ???
    let currentProfileID: Int
    /// ???
    let currentState: Int
    
    enum CodingKeys: String, CodingKey {
        case uid, profiles
        case currentProfileID = "current_profile_id"
        case currentState = "current_state"
    }
}
