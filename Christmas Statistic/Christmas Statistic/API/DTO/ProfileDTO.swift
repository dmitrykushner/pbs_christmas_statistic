//
//  ProfileDTO.swift
//  Christmas Statistic
//
//  Created by Dmitry Kushner on 25.11.21.
//

import Foundation

struct ProfileDTO: Decodable {
    /// Id созданного участника
    let uid: String
    /// ФИО участника
    let fio: String
    /// Дата регистрации
    let regDate: Date?
    /// Выполненные задания
    let completedTasks: [Int]
    /// Массив с ссылками на выполненные задания
    let taskSolutions: TaskSolutions
    
    
    enum CodingKeys: String, CodingKey {
        case uid, fio
        case completedTasks = "completed_tasks"
        case regDate = "reg_date"
        case taskSolutions = "task_solutions"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.uid = try container.decode(String.self, forKey: .uid)
        self.fio = try container.decode(String.self, forKey: .fio)

        let formatter = DateFormatter.yyyyMMddTHHmmssSSSZZZZZZ

        let depTimeString = try? container.decode(String.self, forKey: .regDate)
        self.regDate = formatter.date(from: depTimeString ?? "")
        
        self.completedTasks = (try? container.decode([Int].self, forKey: .completedTasks)) ?? []
        self.taskSolutions = try container.decode(TaskSolutions.self, forKey: .taskSolutions)
    }
}
