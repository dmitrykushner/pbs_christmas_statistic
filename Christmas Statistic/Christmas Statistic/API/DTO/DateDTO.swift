//
//  DateDTO.swift
//  Christmas Statistic
//
//  Created by Dmitry Kushner on 5.01.22.
//

import Foundation

struct DateDTO: Decodable {
    let binary: String?
    let classic: String?
    let unix: Int?
    
    enum CodingKeys: String, CodingKey {
        case binary, classic, unix
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.binary = try container.decode(String.self, forKey: .binary)
        self.classic = try container.decode(String.self, forKey: .classic)
        self.unix = try container.decode(Int.self, forKey: .unix)
    }
}
