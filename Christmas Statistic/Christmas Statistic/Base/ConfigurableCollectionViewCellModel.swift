import UIKit

public typealias CollectionData = (
    section: ConfigurableCollectionViewSectionModel,
    rows: [ConfigurableCollectionViewCellModel]
)

// MARK: - SelectedCollectionViewCellModel

public protocol SelectedCollectionViewCellModel {
    var isSelected: Bool { get }
}

// MARK: - ConfigurableTableViewSectionModel

public protocol ConfigurableCollectionViewSectionModel {
    var viewSectionType: String { get }
}

public extension ConfigurableCollectionViewSectionModel {
    func configure(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let itemCell = NSClassFromString(viewSectionType) as? UICollectionViewCell.Type
        else {
            return UICollectionViewCell()
        }

        let cell = collectionView.dequeue(cell: itemCell, for: indexPath)
        if let item = cell as? UnsafeConfigurable {
            item.make(with: self)
        }

        return cell
    }
}

// MARK: - ConfigurableCollectionViewCellModel

public protocol ConfigurableCollectionViewCellModel {
    var viewCellType: String { get }
    var title: String { get }
    var id: String { get }
}

public extension ConfigurableCollectionViewCellModel {
    func configure(collectionView: UICollectionView, for indexPath: IndexPath) -> UICollectionViewCell {
        guard let itemCell = NSClassFromString(viewCellType) as? UICollectionViewCell.Type
        else {
            return UICollectionViewCell()
        }

        let cell = collectionView.dequeue(cell: itemCell, for: indexPath)

        if let item = cell as? UnsafeConfigurable {
            item.make(with: self)
        }

        if let item = self as? SelectedViewCellModel, item.isSelected {
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: [])
        }

        return cell
    }
}
