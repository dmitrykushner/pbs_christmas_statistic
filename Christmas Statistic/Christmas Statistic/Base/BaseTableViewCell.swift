import Foundation
import UIKit

open class BaseCollectionViewCell: UICollectionViewCell {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.commonInit()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.commonInit()
    }

    private func commonInit() {
        self.setup()
        self.setupUI()
        self.setupConstraints()
    }

    open func setup() {}

    open func setupUI() {}

    open func setupConstraints() {}
}
