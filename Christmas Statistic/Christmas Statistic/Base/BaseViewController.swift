
import UIKit

// MARK: - Constants

private extension GridConstants {
    var viewMaxWidth: CGFloat { 500 }
}

private extension DataConstants {
    var errorTitle: String { "Ошибка" }
}

// MARK: - BaseViewControllerInterface

public protocol BaseViewControllerInterface: ViewControllerInterface, BaseViewControllerProtocol {
    func showMessage(title: String, message: String)
    func showError(_ errorMessage: String)
}

// MARK: - BaseViewControllerProtocol

public protocol BaseViewControllerProtocol {
    func showHud()
    func hideHud()
}

// MARK: - BaseViewController

open class BaseViewController<View, ViewModel>: UIViewController {
    public var contentView: View!
    public var viewModel: ViewModel!

    public lazy var activityIndicatorView: UIView = ActivityIndicatorView(frame: .zero).then {
        $0.pin(
            [
                .self(.width): grid.space48,
                .self(.height): grid.space48
            ]
        )
    }

    override open func viewDidLoad() {
        super.viewDidLoad()

        (self.contentView as? BaseViewProtocol)?.viewLoaded()
        (self.viewModel as? BaseViewModelProtocol)?.viewLoaded()

        self.setup()
        self.setupUI()
        self.setupConstraints()
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        (self.contentView as? BaseViewProtocol)?.viewWillAppear()
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (self.viewModel as? BaseViewModelProtocol)?.viewAppeared()
    }

    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        (self.contentView as? BaseViewProtocol)?.viewWillDisappear()
    }

    open func showHud() {
        view.addSubview(self.activityIndicatorView)
        self.activityIndicatorView.pin(
            [
                .super(.centerX),
                .super(.centerY)
            ]
        )
    }

    open func hideHud() {
        self.activityIndicatorView.removeFromSuperview()
    }

    open func updateLayoutAnimated(animations: (() -> Void)? = nil, complection: (() -> Void)? = nil) {
        UIView.animate(
            withDuration: 0.3,
            animations: {
                animations?()
                self.view.layoutIfNeeded()
            },
            completion: { _ in complection?() }
        )
    }

    open func addHideKeyboardGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    open func setup() {
        view.backgroundColor = apperance.whiteColor

        if let contentView = contentView as? UIView {
            contentView.frame = view.frame
            view.addSubview(contentView)
            contentView.translatesAutoresizingMaskIntoConstraints = false
            contentView.pin(
                [
                    .super(.left, .equal, .defaultHigh),
                    .super(.top),
                    .super(.bottom),
                    .super(.right, .equal, .defaultHigh),
                    .super(.centerX)
                ]
            )
            contentView.pin([.self(.width, .lessThanOrEqual, .required): grid.viewMaxWidth])
        }
        (self.contentView as? BaseViewProtocol)?.setup()
    }

    open func setupUI() {
        (self.contentView as? BaseViewProtocol)?.setupUI()
    }

    open func setupConstraints() {
        (self.contentView as? BaseViewProtocol)?.setupConstraints()
    }

    // MARK: - Actions

    @objc
    open func hideKeyboard() {
        view.endEditing(true)
    }

    // MARK: -

    open func showMessage(title: String, message: String) {
        alert(title, message: message)
    }

    open func showError(_ errorMessage: String) {
        alert(data.errorTitle, message: errorMessage)
    }
}

// MARK: - BaseViewControllerProtocol

extension BaseViewController: BaseViewControllerProtocol {}

// MARK: - BaseViewControllerInterface

extension BaseViewController: BaseViewControllerInterface {}
