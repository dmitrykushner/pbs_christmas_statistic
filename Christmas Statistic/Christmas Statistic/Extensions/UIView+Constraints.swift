import UIKit

// swiftlint:disable all

public enum PinEdgeTo: Hashable {
    public enum Priority: Hashable {
        case `default` // not set
        
        case fittingSizeLevel // 50
        
        case defaultLow // 250
        
        case defaultHigh // 750
        
        case required // 1000
        
        case custom(Float)
    }
    
    public enum Relation {
        case lessThanOrEqual
        
        case equal
        
        case greaterThanOrEqual
    }
    
    /// top relation to super view
    case `self`(
        _ pinEdge: PinEdge,
        _ relation: Relation = .equal,
        _ priority: Priority = .default
    )
    
    /// top relation to super view
    case `super`(
        _ pinEdge: PinEdge,
        _ relation: Relation = .equal,
        _ priority: Priority = .default
    )
    
    /// top relation to other views, if otherPinEdge == nil then otherPinEdge == selfPinEdge
    case other(
        _ selfPinEdge: PinEdge,
        _ otherView: UIView,
        _ otherPinEdge: PinEdge? = nil,
        _ relation: Relation = .equal,
        _ priority: Priority = .default
    )
    
    /// top relation to the views layoutGuide
    case topLayoutGuide(
        _ pinEdge: Relation = .equal,
        _ priority: Priority = .default
    )
    
    /// bottom relation to the views layoutGuide
    case bottomLayoutGuide(
        _ pinEdge: Relation = .equal,
        _ priority: Priority = .default
    )
    
    fileprivate func createConstraint(
        startView: UIView,
        constant: CGFloat,
        multiplier: CGFloat = 1.0
    ) -> NSLayoutConstraint {
        var endItem = self.endItem(startView: startView)
        
        var constraint: NSLayoutConstraint!
        if endItem != nil, self.flippedConstant {
            /* Flip Contraint when the endItem is out of bound */
            constraint = NSLayoutConstraint(
                item: endItem!,
                attribute: self.endAttribute.first!,
                relatedBy: self.relation,
                toItem: startView,
                attribute: self.startAttribute.first!,
                multiplier: CGFloat(multiplier),
                constant: CGFloat(constant)
            )
        } else {
            constraint = NSLayoutConstraint(
                item: startView,
                attribute: self.startAttribute.first!,
                relatedBy: self.relation,
                toItem: endItem,
                attribute: self.endAttribute.first!,
                multiplier: CGFloat(multiplier),
                constant: CGFloat(constant)
            )
        }
        if let priority = priority {
            constraint.priority = priority
        }
        return constraint
    }
}

private extension PinEdgeTo {
    var startAttribute: [NSLayoutConstraint.Attribute] {
        switch self {
        case .self(let pinEdge, _, _),
             .super(let pinEdge, _, _),
             .other(let pinEdge, _, _, _, _):
            switch pinEdge {
            case .left:
                return [.leading, .left]
                
            case .right:
                return [.trailing, .right]
                
            case .bottom:
                return [.bottom]
                
            case .top:
                return [.top]
                
            case .centerX:
                return [.centerX]
                
            case .centerY:
                return [.centerY]
                
            case .width:
                return [.width]
                
            case .height:
                return [.height]
            }
        case .topLayoutGuide:
            return [.bottom]
            
        case .bottomLayoutGuide:
            return [.top]
        }
    }
    
    var endAttribute: [NSLayoutConstraint.Attribute] {
        switch self {
        case .self(let pinEdge, _, _):
            guard pinEdge == .width || pinEdge == .height else {
                return endAttribute(with: pinEdge)
            }
            return [.notAnAttribute]
            
        case .super(let pinEdge, _, _):
            return endAttribute(with: pinEdge)
            
        case .other(let pinEdge, _, let otherPinEdge, _, _):
            return endAttribute(with: otherPinEdge ?? pinEdge)
            
        case .topLayoutGuide:
            return [.bottom]
            
        case .bottomLayoutGuide:
            return [.top]
        }
    }
    
    private func endAttribute(with pinEdge: PinEdge) -> [NSLayoutConstraint.Attribute] {
        switch pinEdge {
        case .left:
            return [.leading, .left]
            
        case .right:
            return [.trailing, .right]
            
        case .bottom:
            return [.bottom]
            
        case .top:
            return [.top]
            
        case .centerX:
            return [.centerX]
            
        case .centerY:
            return [.centerY]
            
        case .width:
            return [.width]
            
        case .height:
            return [.height]
        }
    }
    
    func defaultConstantFor(view: UIView) -> CGFloat {
        switch self {
        case .self(let pinEdge, _, _), .super(let pinEdge, _, _):
            return self.defaultConstantFor(with: view, pinEdge: pinEdge)
            
        case .other(let pinEdge, _, let otherPinEdge, _, _):
            return self.defaultConstantFor(with: view, pinEdge: otherPinEdge ?? pinEdge)

        default:
            return 0.0
        }
    }
    
    private func defaultConstantFor(with view: UIView, pinEdge: PinEdge) -> CGFloat {
        switch pinEdge {
        case .width:
            return CGFloat(view.frame.width)
            
        case .height:
            return CGFloat(view.frame.height)
            
        default:
            return 0.0
        }
    }
    
    func endItem(startView: UIView) -> AnyObject? {
        switch self {
        case .super:
            return startView.superview
            
        case .other(_, let otherView, _, _, _):
            return otherView
            
        case .bottomLayoutGuide:
            return startView.parentViewController?.bottomLayoutGuide
            
        case .topLayoutGuide:
            return startView.parentViewController?.topLayoutGuide
            
        default:
            return nil
        }
    }
    
    var flippedConstant: Bool {
        switch self {
        case .super(let pinEdge, _, _), .other(let pinEdge, _, _, _, _):
            switch pinEdge {
            case .right, .bottom:
                return true
                
            default:
                return false
            }
            
        case .bottomLayoutGuide:
            return true
            
        default:
            return false
        }
    }
    
    var relation: NSLayoutConstraint.Relation {
        switch self {
        case .self(_, let relation, _),
             .super(_, let relation, _),
             .other(_, _, _, let relation, _),
             .bottomLayoutGuide(let relation, _),
             .topLayoutGuide(let relation, _):
            
            switch relation {
            case .equal:
                return .equal
                
            case .lessThanOrEqual:
                return .lessThanOrEqual
                
            case .greaterThanOrEqual:
                return .greaterThanOrEqual
            }
        }
    }
    
    var priority: UILayoutPriority? {
        switch self {
        case .self(_, _, let priority),
             .super(_, _, let priority),
             .other(_, _, _, _, let priority),
             .bottomLayoutGuide(_, let priority),
             .topLayoutGuide(_, let priority):
            
            switch priority {
            case .default:
                return nil
                
            case .fittingSizeLevel:
                return UILayoutPriority.fittingSizeLevel
            
            case .defaultLow:
                return UILayoutPriority.defaultLow
            
            case .defaultHigh:
                return UILayoutPriority.defaultHigh
            
            case .required:
                return UILayoutPriority.required
            
            case .custom(let value):
                return UILayoutPriority(rawValue: value)
            }
        }
    }
}

/**
 * This enum represents all current options for adding constraints
 * .left, .right, .bottom, .top, .centerX, .centerY are relations to the views superview
 * .width, .height are relations to the view itself
 * .leftTo(UIView), .rightTo(UIView), .bottomTo(UIView), .topTo(UIView), .equalWidth(UIView), .equalHeight(UIView) are relations from one view to another views
 */

public enum PinEdge: Hashable {
    /// leading (left) relation to the views superview
    case left
    
    /// trailing (right) relation to the views superview
    case right
    
    /// bottom relation to the views superview
    case bottom
    
    /// top relation to the views superview
    case top
    
    /// center x relation to the views superview
    case centerX
    
    /// center y relation to the views superview
    case centerY
    
    /// width relation for the view
    case width
    
    /// height relation for the view
    case height
    
    /**
     * Compare two PinEdge's
     *
     * - Parameter lhs: first PinEdge
     * - Parameter rhs: first PinEdge
     * - Returns: Boolean
     */
    public static func == (lhs: PinEdge, rhs: PinEdge) -> Bool {
        lhs.hashValue == rhs.hashValue
    }
    
    /**
     * Unique value for a PinEdge, mainly used for comparising
     *
     * - Returns: Int
     */
    public var hashValue: Int {
        switch self {
        case .left:
            return 1
            
        case .right:
            return 2
            
        case .bottom:
            return 3
            
        case .top:
            return 4
            
        case .centerX:
            return 5
            
        case .centerY:
            return 6
            
        case .width:
            return 7
            
        case .height:
            return 8
        }
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            guard let newResponder = parentResponder?.next else {
                break
            }
            if let viewController = newResponder as? UIViewController {
                return viewController
            }
            parentResponder = newResponder
        }
        return nil
    }
}

/* Add Constraints */
public extension UIView {
    /**
     * Add multiple NSLayoutConstraints to a view, provided in an array.
     * The NSLayoutConstraints value's are calculated from the views frame.
     * Make sure the view is first added to a superview.
     *
     * - Parameter edges: [PinEdge], Array of PinEdge's. Take a look at PinEdge for all options
     */
    func pin(_ edges: [PinEdgeTo]) {
        edges.forEach { edge in
            _ = pin(edge)
        }
    }
    
    /**
     * Add multiple NSLayoutConstraints to a view, provided in an dictionary.
     * If the provided value is Nil, then the NSLayoutConstraints value will be calculated from the views frame.
     * Make sure the view is first added to a superview.
     *
     * - Parameter edges: [PinEdge:CGFloat?], Dicionary of PinEdge's and constant values. Take a look at PinEdge for all options
     */
    func pin(_ edges: [PinEdgeTo: CGFloat?]) {
        edges.forEach { edge, constant in
            _ = pin(edge, constant: constant)
        }
    }
    
    /**
     * Add a NSLayoutConstraint to a view.
     * Make sure the view is first added to a superview.
     *
     * - Parameter edge: PinEdge, Take a look at PinEdge for all options
     * - Parameter constant: Float?, The amount of spacing in pixels that will be used by the Constrant. If the provided value is Nil, then the NSLayoutConstraints value will be calculated from the views frame
     * - Parameter multiplier: Float, modify the default constraint multiplier
     * - Returns: The created NSLayoutConstraint
     */
    func pin(
        _ edge: PinEdgeTo,
        constant: CGFloat? = nil,
        multiplier: CGFloat = 1.0
    ) -> NSLayoutConstraint {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let constraint = edge.createConstraint(
            startView: self,
            constant: constant ?? edge.defaultConstantFor(view: self),
            multiplier: multiplier
        )
               
        if constraint.secondItem == nil {
            self.addConstraint(constraint)
        } else {
            self.superview?.addConstraint(constraint)
        }

        return constraint
    }
    
    func pinToSuperview(
        top: CGFloat = 0,
        left: CGFloat = 0,
        right: CGFloat = 0,
        bottom: CGFloat = 0
    ) {
        self.pin([
            .super(.top): top,
            .super(.left): left,
            .super(.right): right,
            .super(.bottom): bottom
        ])
    }
}

/* Find Constraints */
public extension UIView {
    /**
     * Find a NSLayoutConstraint for a UIView (or a subclass of UIView)
     *
     * - Parameter edge: PinEdge, Take a look at PinEdge enum for all options
     */
    func constraint(_ edge: PinEdgeTo) -> NSLayoutConstraint? {
        let attributes = edge.startAttribute
        let endItem = edge.endItem(startView: self)
        let matches = attributes.flatMap(self.searchConstaint(endItem: endItem))
        return matches.first
    }
    
    private func searchConstaint(endItem: AnyObject?) -> ((NSLayoutConstraint.Attribute) -> (NSLayoutConstraint?)) {
        return { attribute in
            let constraints = (attribute == .width || attribute == .height) ? self.constraints : self.superview?.constraints
            return constraints?.filter(self.isConstraintWith(attribute: attribute, endItem: endItem)).first
        }
    }
    
    private func isConstraintWith(
        attribute: NSLayoutConstraint.Attribute,
        endItem: AnyObject?
    ) -> ((NSLayoutConstraint) -> (Bool)) {
        return { constraint in
            
            guard constraint.relation == .equal else {
                return false
            }
            
            /* Default case validation - UIView */
            if let firstItem = constraint.firstItem as? UIView,
               firstItem == self, constraint.firstAttribute == attribute
            {
                if let secondItem = constraint.secondItem as? UIView,
                   secondItem == endItem as? UIView
                {
                    return true
                } else if endItem == nil,
                          constraint.secondItem == nil
                {
                    return true
                }
            }
            
            /* Flipped case validation */
            if let firstItem = constraint.firstItem as? UIView,
               let secondItem = constraint.secondItem as? UIView,
               firstItem == endItem as? UIView,
               constraint.secondAttribute == attribute,
               secondItem == self
            {
                return true
            }
            
            return false
        }
    }
}

/* Animate Constraints, perform on a parent view */
public extension UIViewController {
    /**
     * Perform NSLayoutConstraint changes with a animation
     *
     * - Parameter duration: TimeInterval, The duration of the animation
     * - Parameter constraints: Void completion block in which NSLayoutConstraint constant changes need to be performed
     * - Parameter animations: (Optional) Void completion block in which non NSLayoutConstraint changes need to be performed
     * - Parameter completion: (Optional) Bool completion block which gets performed after the animation
     */
    func animateConstraints(
        duration: TimeInterval,
        constraints: () -> Void,
        animations: (() -> Void)? = nil,
        completion: ((Bool) -> Void)? = nil
    ) {
        self.view.animateConstraints(
            duration: duration,
            constraints: constraints,
            animations: animations,
            completion: completion
        )
    }
}

public extension UIView {
    /**
     * Perform NSLayoutConstraint changes with a animation, perform this call on the highest UIView or use the extension on UIViewController
     *
     * - Parameter duration: TimeInterval, The duration of the animation
     * - Parameter constraints: Void completion block in which NSLayoutConstraint constant changes need to be performed
     * - Parameter animations: (Optional) Void completion block in which non NSLayoutConstraint changes need to be performed
     * - Parameter completion: (Optional) Bool completion block which gets performed after the animation
     */
    func animateConstraints(
        duration: TimeInterval,
        constraints: () -> Void,
        animations: (() -> Void)? = nil,
        completion: ((Bool) -> Void)? = nil
    ) {
        constraints()
        self.setNeedsLayout()
        UIView.animate(withDuration: duration, animations: {
            animations?()
            self.layoutIfNeeded()
        }, completion: completion)
    }
    
    func removeAllConstraints() {
        var _superview = self.superview
        
        while let superview = _superview {
            for constraint in superview.constraints {
                if let first = constraint.firstItem as? UIView, first == self {
                    superview.removeConstraint(constraint)
                }
                
                if let second = constraint.secondItem as? UIView, second == self {
                    superview.removeConstraint(constraint)
                }
            }
            
            _superview = superview.superview
        }
        
        self.removeConstraints(self.constraints)
        self.translatesAutoresizingMaskIntoConstraints = true
    }
}

public extension NSLayoutConstraint {
    /**
     * Remove a NSLayoutConstraint from the managing view.
     */
    func remove() {
        if let secondItem = self.secondItem {
            secondItem.removeConstraint(self)
        } else {
            self.firstItem?.removeConstraint(self)
        }
    }
}

// swiftlint:enable all
