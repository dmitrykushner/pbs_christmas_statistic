//
//  SceneDelegate.swift
//  Christmas Statistic
//
//  Created by Dmitry Kushner on 25.11.21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    private var coordinator: Coordinator!
    private var assembly: BaseAssembly!

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = scene as? UIWindowScene else { return }
        
        let window = UIWindow(windowScene: windowScene)

        let assembly = MainAssemblyImpl()
        coordinator = assembly.coordinator()
        
        window.rootViewController = self.coordinator.rootViewController
        self.window = window
        window.makeKeyAndVisible()
        self.coordinator.start()
        
        self.assembly = assembly

    }

    // MARK: -

    func sceneDidDisconnect(_: UIScene) {}

    func sceneDidBecomeActive(_: UIScene) {}

    func sceneWillResignActive(_: UIScene) {}

    func sceneWillEnterForeground(_: UIScene) {}

    func sceneDidEnterBackground(_: UIScene) {}
}

