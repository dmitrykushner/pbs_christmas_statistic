
import UIKit

final class MainCoordinator: BaseCoordinator {
    private weak var assembly: MainAssembly?

    init(assembly: MainAssembly) {
        self.assembly = assembly

        super.init(navigationController: UINavigationController())
    }

    override func start() {
        guard let item = assembly?.makeMain(output: self) else { return }
        navigationController.pushViewController(item, animated: true)
    }
    
    
}

// MARK: - MainOutputInterface

extension MainCoordinator: MainOutputInterface {
    func showInformationController(childrenName: String, taskSolutions: [String : AnyObject]?) {
        let childrenTaskAssebly = ChildrenTaskAssemblyImpl()
        let childrenTaskCoordinator = ChildrenTaskCoordinator(
            assembly: childrenTaskAssebly,
            navigationController: navigationController,
            title: childrenName,
            taskSolutions: taskSolutions
        )
        add(child: childrenTaskCoordinator)
        childrenTaskCoordinator.start()
    }
}
