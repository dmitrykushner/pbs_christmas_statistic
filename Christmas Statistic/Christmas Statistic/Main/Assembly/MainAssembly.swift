import UIKit

// MARK: - MainAssembly

protocol MainAssembly: BaseAssembly {
    func makeMain(output: MainOutputInterface?) -> MainViewController
}

// MARK: - MainAssemblyImpl

final class MainAssemblyImpl: MainAssembly {

    func coordinator() -> BaseCoordinator {
        MainCoordinator(assembly: self)
    }
}

// MARK: -

extension MainAssemblyImpl {
    func makeMain(output: MainOutputInterface?) -> MainViewController {
        MainSceneAssembly(
            config: MainConfigModel(
                output: output,
                apiService: ApiServiceImpl()
            )
        ).controller as! MainViewController
    }
}
