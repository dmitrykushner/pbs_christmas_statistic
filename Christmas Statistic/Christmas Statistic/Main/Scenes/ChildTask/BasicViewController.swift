import UIKit
import SnapKit

class BasicViewController:UIViewController {
    
    lazy var imageView:UIImageView = {
        let iv = UIImageView()
        
        // Setup Image Viewer
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    override func loadView() {
        super.loadView()
        view = UIView()
        view.backgroundColor = .white
        view.addSubview(imageView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeConstraints()
    }

    func makeConstraints() {
        imageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: UIScreen.main.bounds.width - 10, height: UIScreen.main.bounds.height - 50))
        }
    }
}

extension BasicViewController {
    struct ViewModel {
        let image: UIImage
    }
    
    func configure(with viewModel: BasicViewController.ViewModel) {
        imageView.image = viewModel.image
    }
}
