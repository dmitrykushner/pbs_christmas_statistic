
import UIKit
import AVKit

final class ChildrenTaskCoordinator: BaseCoordinator {
    private weak var assembly: ChildrenTaskAssembly?
    private var title: String?
    private var taskSolutions: [String: AnyObject]?

    init(
        assembly: ChildrenTaskAssembly,
        navigationController: UINavigationController? = nil,
        title: String?,
        taskSolutions: [String: AnyObject]?
    ) {
        self.assembly = assembly
        self.title = title
        self.taskSolutions = taskSolutions
        
        super.init(navigationController: navigationController ?? UINavigationController())
    }

    override func start() {
        guard let item = assembly?.makeChildrenTask(
            output: self,
            title: title ?? "",
            taskSolutions: taskSolutions
        ) else { return }
        navigationController.pushViewController(item, animated: true)
    }
}

// MARK: - MainOutputInterface

extension ChildrenTaskCoordinator: ChildrenTaskOutputInterface {
    func showVideoController(with videoURL: URL) {
        let player = AVPlayer(url: videoURL)
        
        let vc = AVPlayerViewController()
        vc.player = player
        
        navigationController.present(vc, animated: true) { vc.player?.play() }
    }
    
    func showPhotoBaseController(with image: UIImage) {
        let vc = BasicViewController().then { $0.configure(with: .init(image: image)) }
        navigationController.pushViewController(vc, animated: true)
    }
}
