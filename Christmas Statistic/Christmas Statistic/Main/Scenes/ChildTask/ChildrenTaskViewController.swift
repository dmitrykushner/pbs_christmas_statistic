
import UIKit
import NVActivityIndicatorView

// MARK: - Constants


// MARK: - MainViewInterface

protocol ChildrenTaskViewControllerInterface: BaseViewControllerInterface {
    func addChild(_ child: UIViewController)
    func setTitle(_ title: String)
    
    func stopAnimating()
    func changeLoadingText(loadingDay: String)
}

// MARK: - MainViewController

final class ChildrenTaskViewController: BaseViewController<ChildrenTaskViewInterface, ChildrenTaskViewModelInterface> {
    
    private lazy var activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    private lazy var loadingLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 20)).then {
        $0.font = UIFont.systemFont(ofSize: 12)
        $0.text = "1й"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - Setup

    override func setup() {
        super.setup()
        addSubviews()
        guard let contentView = contentView as? UIView else { return }
        contentView.removeAllConstraints()

        view.backgroundColor = .white

        view.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            contentView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            contentView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)
        ])
    }
    
    private func addSubviews() {
        let barButton = UIBarButtonItem(customView: activityIndicator)
        let labelButton = UIBarButtonItem(customView: loadingLabel)
        self.navigationItem.setRightBarButtonItems([labelButton, barButton], animated: true)
        activityIndicator.startAnimating()
    }
}

// MARK: - ChildrenTaskViewController

extension ChildrenTaskViewController: ChildrenTaskViewControllerInterface {
    func changeLoadingText(loadingDay: String) {
        loadingLabel.text = loadingDay
    }
    
    func stopAnimating() {
        activityIndicator.stopAnimating()
    }
    
    func setTitle(_ title: String) {
        self.title = title
    }
}
