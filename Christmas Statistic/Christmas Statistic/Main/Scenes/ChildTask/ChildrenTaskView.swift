
import UIKit

// MARK: - MainViewInterface

public protocol ChildrenTaskViewInterface: BaseViewInterface {
    func reloadData()
}

// MARK: - MainView

class ChildrenTaskView: BaseView<ChildrenTaskViewModelInterface> {
    
    private lazy var tableView = UITableView().then {
        $0.delegate = self
        $0.dataSource = self
        $0.register(InfoTableViewCell.self, forCellReuseIdentifier: "InfoTableViewCell")
        $0.register(InfoHeaderView.self, forHeaderFooterViewReuseIdentifier: "InfoHeaderView")
    }
    
    // MARK: - Setup

    override func setup() {
    }

    override func setupUI() {
        addSubview(tableView)
    }

    override func setupConstraints() {
        self.tableView.pinToSuperview()
    }
}

// MARK: - ChildrenTaskView

extension ChildrenTaskView: ChildrenTaskViewInterface {
    func reloadData() {
        tableView.reloadData()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension ChildrenTaskView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.sections[section].cellModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell") as? InfoTableViewCell
        let model = viewModel.sections[indexPath.section].cellModels[indexPath.row]
        cell?.configure(with: model)
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        viewModel.didSelect(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "InfoHeaderView") as? InfoHeaderView
        tableHeaderView?.configure(with: viewModel.sections[section].headerModel)
        return tableHeaderView ?? UIView()
    }
}
