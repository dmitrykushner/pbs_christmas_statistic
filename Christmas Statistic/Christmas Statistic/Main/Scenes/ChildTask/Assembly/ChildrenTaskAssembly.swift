import UIKit

// MARK: - ChildrenTaskAssembly

protocol ChildrenTaskAssembly: BaseAssembly {
    func makeChildrenTask(
        output: ChildrenTaskOutputInterface?,
        title: String,
        taskSolutions: [String: AnyObject]?
    ) -> ChildrenTaskViewController
}

// MARK: - ChildrenTaskAssemblyImpl

final class ChildrenTaskAssemblyImpl: ChildrenTaskAssembly {
    
    func coordinator() -> BaseCoordinator {
        ChildrenTaskCoordinator(
            assembly: self,
            navigationController: nil,
            title: nil,
            taskSolutions: nil
        )
    }
}

// MARK: -

extension ChildrenTaskAssemblyImpl {
    func makeChildrenTask(
        output: ChildrenTaskOutputInterface?,
        title: String,
        taskSolutions: [String: AnyObject]?
    ) -> ChildrenTaskViewController {
        ChildrenTaskSceneAssembly(
            config: .init(
                output: output,
                title: title,
                taskSolutions: taskSolutions
            )
        ).controller!
    }
}
