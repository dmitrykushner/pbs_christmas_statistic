
import Foundation
import UIKit


// MARK: - ChildrenTaskInputInterface

protocol ChildrenTaskInputInterface: BaseInputInterface {
}

// MARK: - ChildrenTaskOutputInterface

protocol ChildrenTaskOutputInterface: BaseOutputInterface {
    func showVideoController(with videoURL: URL)
    func showPhotoBaseController(with image: UIImage)
}

// MARK: - ChildrenTaskConfigModel

final class ChildrenTaskConfigModel: BaseConfigModel<ChildrenTaskInputInterface, ChildrenTaskOutputInterface> {
    
    var title: String
    var taskSolutions: [Dictionary<String, AnyObject>.Element]?
    
    init(
        output: ChildrenTaskOutputInterface?,
        title: String,
        taskSolutions: [String : AnyObject]?
    ) {
        self.title = title
        self.taskSolutions = taskSolutions?.sorted { Int($0.key) ?? .zero < Int($1.key) ?? .zero }
        super.init(output: output)
    }
}
