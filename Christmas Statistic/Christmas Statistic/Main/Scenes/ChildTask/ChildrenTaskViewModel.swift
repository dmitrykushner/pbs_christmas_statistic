import UIKit
import AVFAudio
import AVKit

// MARK: - MainViewModelInterface

protocol ChildrenTaskViewModelInterface: BaseViewModelInterface {
    var sections: [Section] { get set }
    func didSelect(at indexPath: IndexPath)
}

// MARK: - MainViewController

struct Section {
    var headerModel: InfoHeaderView.ViewModel
    var cellModels: [InfoTableViewCell.ViewModel]
}

final class ChildrenTaskViewModel: BaseViewModel<ChildrenTaskViewControllerInterface, ChildrenTaskViewInterface, ChildrenTaskConfigModel> {
    
    // MARK: - Public variables
    var sections: [Section] = []
    
    // MARK: - Private variables
    private var allDates: [[String: AnyObject]]?
    private var isNeedRemakeSection = true

    override func viewLoaded() {
        super.viewLoaded()
        getDateAllDates()
        controller.setTitle(config.title)
    }
    
    override func viewAppeared() {
        super.viewAppeared()
        if isNeedRemakeSection {
            controller.showHud()
            self.makeSections()
        }
    }
    
    private func makeSections() {
        let group = DispatchGroup()
        var sections = [Section]()
        var cellsModels = [InfoTableViewCell.ViewModel]()
        self.isNeedRemakeSection = false
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            self.config.taskSolutions?.enumerated().forEach { taskSolution in
                print("Start \(taskSolution.element.key) section making")
                (taskSolution.element.value as? [String])?.forEach { linkString in
                    print(taskSolution.element.key)
                    group.enter()
                    cellsModels.append(.init(
                        linkString: linkString,
                        image: self.downloadImage(
                            from: linkString.replacingOccurrences(of: "/home/ubuntu/pbsbot", with: "https://pbs-bot.hackcat.ml/"),
                            group: group),
                        dateString: self.allDates?.first { $0["linkString"] as? String == linkString }?["classic"] as? String ?? "-"
                    ))
                }
                sections.append(.init(headerModel: .init(title: "\(taskSolution.element.key)й день"), cellModels: cellsModels))
                self.sections.append(contentsOf: sections)
                sections.removeAll()
                cellsModels.removeAll()
                print("Finished \(taskSolution.element.key) section making")
                DispatchQueue.main.async {
                    if self.controller != nil {
                        if self.sections.count == self.config.taskSolutions?.count {
                            self.controller.stopAnimating()
                            self.controller.changeLoadingText(loadingDay: "")
                        } else {
                            self.controller.changeLoadingText(loadingDay: "\(taskSolution.offset + 2)й")
                            self.controller.hideHud()
                        }
                        self.view.reloadData()
                    }
                }
            }
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    private func downloadImage(from linkString: String, group: DispatchGroup) -> UIImage? {
        if controller != nil {
            let lowerCasedLinkString = linkString.lowercased()
            if !lowerCasedLinkString.contains("mp4") && !lowerCasedLinkString.contains("mov")
            {
                print("Download Started")
                guard let url = URL(string: linkString) else { return  nil }
                guard let data = try? Data(contentsOf: url) else { return nil }
                group.leave()
                return UIImage(data: data)
            } else {
                print("It is video \(linkString)")
                group.leave()
                return nil
            }
        } else {
            return nil
        }
    }

    private func getDateAllDates() {
        if let path = Bundle.main.path(forResource: "timedata", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String: [String: AnyObject]]
                
                var newDateDictionary = [[String: AnyObject]]()
                jsonObj?.forEach {
                    var newValuesArray = $0.value
                    newValuesArray.updateValue($0.key as AnyObject, forKey: "linkString")
                    newDateDictionary.append(newValuesArray)
                }
                
                allDates = newDateDictionary
            } catch let error {
                print("ОШИБКА")
            }
        } else {
            print("Invalid filename/path.")
        }
    }
}

// MARK: - MainViewModelInterface

extension ChildrenTaskViewModel: ChildrenTaskViewModelInterface {
    func didSelect(at indexPath: IndexPath) {
        if !isNeedRemakeSection {
            if let image = sections[indexPath.section].cellModels[indexPath.row].image {
                config.output?.showPhotoBaseController(with: image)
            } else {
                guard let video = URL(string: sections[indexPath.section].cellModels[indexPath.row].linkString.replacingOccurrences(of: "/home/ubuntu/pbsbot", with: "https://pbs-bot.hackcat.ml")) else { return }
                config.output?.showVideoController(with: video)
            }
        }
    }
}

// MARK: - MainInputInterface

extension ChildrenTaskViewModel: ChildrenTaskInputInterface {}

