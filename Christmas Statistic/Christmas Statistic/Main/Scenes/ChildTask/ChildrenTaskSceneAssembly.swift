
// MARK: - ChildrenTaskSceneAssembly

final class ChildrenTaskSceneAssembly: BaseSceneAssembly<
ChildrenTaskViewController,
ChildrenTaskView,
ChildrenTaskViewModel,
ChildrenTaskConfigModel
> {}
