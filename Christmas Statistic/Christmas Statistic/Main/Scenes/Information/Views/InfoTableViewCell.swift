//
//  InfoTableViewCell.swift
//  Christmas Statistic
//
//  Created by Dmitry Kushner on 3.01.22.
//

import UIKit
import SnapKit

class InfoTableViewCell: UITableViewCell {
    private lazy var titleLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        $0.textAlignment = .center
        $0.numberOfLines = 0
    }
    
    private lazy var dayImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }
    
    private lazy var dateLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        $0.textAlignment = .center
        $0.textColor = .red
    }

    private lazy var videoBlackView = UIView().then {
        $0.backgroundColor = .black
        $0.isHidden = true
    }

    private lazy var playIconImageView = UIImageView().then {
        $0.image = UIImage(named: "Play")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        selectionStyle = .none
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(dayImageView)
        addSubview(titleLabel)
        addSubview(dateLabel)
        addSubview(videoBlackView)
        videoBlackView.addSubview(playIconImageView)
    }
    
    private func makeConstraints() {
        dateLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(grid.space12)
            make.centerX.equalToSuperview()
        }
        dayImageView.snp.makeConstraints { make in
            make.top.equalTo(dateLabel.snp.bottom).offset(grid.space4)
            make.bottom.leading.trailing.equalToSuperview().inset(grid.space12)
            make.height.equalTo(400)
        }
        videoBlackView.snp.makeConstraints { make in
            make.top.equalTo(dateLabel.snp.bottom).offset(grid.space4)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(250)
        }
        playIconImageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: grid.space24, height: grid.space24))
        }
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(videoBlackView.snp.bottom).offset(grid.space2)
            make.bottom.leading.trailing.equalToSuperview().inset(grid.space2)
        }
    }
}

extension InfoTableViewCell {
    struct ViewModel {
        let linkString: String
        let image: UIImage?
        let dateString: String
    }
    
    func configure(with viewModel: InfoTableViewCell.ViewModel) {
        dayImageView.isHidden = viewModel.image == nil
        dayImageView.image = viewModel.image
        titleLabel.text = "Если по нажатии на видео оно не загрузится - смотри Telegram."
        dateLabel.text = viewModel.dateString
        titleLabel.isHidden = viewModel.image != nil
        videoBlackView.isHidden = viewModel.image != nil
    }
}
