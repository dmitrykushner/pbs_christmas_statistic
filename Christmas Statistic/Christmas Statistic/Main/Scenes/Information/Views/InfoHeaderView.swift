//
//  InfoHeaderView.swift
//  Christmas Statistic
//
//  Created by Dmitry Kushner on 3.01.22.
//

import UIKit

final class InfoHeaderView: UITableViewHeaderFooterView {
    private lazy var titleLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 16)
        $0.textColor = .black
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(titleLabel)
    }
    
    private func makeConstraints() {
        titleLabel.pinToSuperview(top: 2, left: 16, right: 16, bottom: 2)
    }
}

extension InfoHeaderView {
    struct ViewModel {
        let title: String
    }
    
    func configure(with viewModel: InfoHeaderView.ViewModel) {
        titleLabel.text = viewModel.title
    }
}
