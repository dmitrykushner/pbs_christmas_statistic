import Foundation

struct MainDataViewModel: ConfigurableCollectionViewCellModel {
    enum TitleAlighment {
        case center, left
    }
    
    var viewCellType: String {
        NSStringFromClass(MainCollectionViewCell.self)
    }

    let id: String
    
    let title: String
    let alighment: TitleAlighment

    init(id: String, title: String, alighment: TitleAlighment) {
        self.id = id
        self.title = title
        self.alighment = alighment
    }
}
