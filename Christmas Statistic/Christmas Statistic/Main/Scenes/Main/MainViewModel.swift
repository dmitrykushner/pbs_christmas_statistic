import UIKit

// MARK: - MainViewModelInterface

protocol MainViewModelInterface: BaseViewModelInterface {
    var data: [ConfigurableCollectionViewCellModel] { get }
    
    func search(searchText: String)
    func didSelect(at indexPath: IndexPath)
}

// MARK: - MainViewController

final class MainViewModel: BaseViewModel<MainViewControllerInterface, MainViewInterface, MainConfigModel> {
    
    var data: [ConfigurableCollectionViewCellModel] = []
    var originalData: [[MainDataViewModel]] = []
    var profiles: [ProfileDTO] = []

    private var searchText: String = ""

    override func viewLoaded() {
        super.viewLoaded()
        
        loadingProcess(.inProcess)
        config.apiService.getUserStatistic { [weak self] result in
            self?.loadingProcess(.ended)
            switch result {
            case .value(let items):
                self?.configureData(items.compactMap { $0.value })
            case .error(let error):
                self?.controller.showError(error.localizedDescription)
            }
        }
    }
    
    func configureData(_ data: [TelegramUserDTO]) {
        profiles = data.compactMap {
            $0.profiles
        }.reduce([], +)
        var data = data.compactMap { item in
            item.profiles
        }.reduce([], +)
        data = data.sorted(by: { $0.fio > $1.fio })
        data = data.sorted(by: { lhs, rhs in
            if lhs.completedTasks.count != rhs.completedTasks.count {
                return lhs.completedTasks.count > rhs.completedTasks.count
            } else {
                return lhs.fio < rhs.fio
            }
        })

        self.originalData.append([
            [MainDataViewModel(id: "", title: "№", alighment: .center)],
            [MainDataViewModel(id: "", title: "ФИО", alighment: .center)],
            (1...25).compactMap {
                MainDataViewModel(id: "", title: "\($0)", alighment: .center)
            }
        ].reduce([], +))
        
        self.originalData.append(contentsOf: data.enumerated().compactMap { item in
            let profile = item.element
            var cells = [
                MainDataViewModel(id: "", title: "\(item.offset + 1)", alighment: .center),
                MainDataViewModel(id: profile.uid, title: profile.fio, alighment: .left)
            ]
            (1...25).forEach {
                cells.append(MainDataViewModel(id: profile.uid, title: profile.completedTasks.contains($0) ? "✅" : "", alighment: .center))
            }
            return cells
        })
        self.data = self.originalData.reduce([], +)
        
        view.reloadData()
    }
    
    private func setupData(searchText: String) {
        self.searchText = searchText
    
        // Filter by searched text of needed
        guard !searchText.isEmpty else {
            self.data = originalData.reduce([], +)
            view.reloadData()
            return
        }

        self.data = originalData.enumerated().filter {
            guard $0.offset != 0 else { return true }
            return $0.element[1].title.range(of: searchText, options: .caseInsensitive) != nil
        }.compactMap { $0.element }.reduce([], +)
        view.reloadData()
    }
}

// MARK: - MainViewModelInterface

extension MainViewModel: MainViewModelInterface {
    func didSelect(at indexPath: IndexPath) {
        let tappedChildrenName = data[indexPath.row].title
        let tappedChildrenUID = data[indexPath.row].id
        let taskSolutions = profiles.first { $0.fio == tappedChildrenName && $0.uid == tappedChildrenUID }?.taskSolutions.toDictionary()
        if
            !tappedChildrenName.isEmpty,
            tappedChildrenName != "✅",
            Int(tappedChildrenName) == nil,
            tappedChildrenName != "ФИО",
            tappedChildrenName != "№" {
            config.output?.showInformationController(childrenName: tappedChildrenName, taskSolutions: taskSolutions)
        }
    }
    
    internal func search(searchText: String) {
        self.setupData(searchText: searchText)
    }
}

// MARK: - MainInputInterface

extension MainViewModel: MainInputInterface {}

