
import UIKit
import NVActivityIndicatorView

// MARK: - Constants


// MARK: - MainViewInterface

protocol MainViewControllerInterface: BaseViewControllerInterface {
    func addChild(_ child: UIViewController)
}

// MARK: - MainViewController

final class MainViewController: BaseViewController<MainViewInterface, MainViewModelInterface> {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: - Setup

    override func setup() {
        super.setup()
        guard let contentView = contentView as? UIView else { return }
        contentView.removeAllConstraints()

        view.backgroundColor = .white

        view.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            contentView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            contentView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)
        ])
    }
}

// MARK: - MainViewController

extension MainViewController: MainViewControllerInterface {}
