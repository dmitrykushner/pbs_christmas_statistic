import UIKit

// MARK: - MainCollectionViewCell

class MainCollectionViewCell: BaseCollectionViewCell {
    private var viewModel: MainDataViewModel!

    private lazy var nameTextLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 16)
    }

    override func setupUI() {
        super.setupUI()

        contentView.addSubview(self.nameTextLabel)
        
        backgroundColor = .white
    }

    override func setupConstraints() {
        super.setupConstraints()
        self.nameTextLabel.pinToSuperview(top: 0, left: grid.space4, right: grid.space4, bottom: 0)
    }
}

// MARK: - Configure

extension MainCollectionViewCell: ConfigurableView {
    func configure(with viewModel: MainDataViewModel) {
        self.viewModel = viewModel
        
        nameTextLabel.text = viewModel.title
        switch viewModel.alighment {
        case .left:
            nameTextLabel.textAlignment = .left
        case .center:
            nameTextLabel.textAlignment = .center
        }
    }
}
