
import Foundation


// MARK: - MainInputInterface

protocol MainInputInterface: BaseInputInterface {}

// MARK: - MainOutputInterface

protocol MainOutputInterface: BaseOutputInterface {
    /// Show controller with information about children data
    func showInformationController(childrenName: String, taskSolutions: [String: AnyObject]?)
}

// MARK: - MainInputInterface

final class MainConfigModel: BaseConfigModel<MainInputInterface, MainOutputInterface> {

    var apiService: ApiService

    init(
        output: MainOutputInterface?,
        apiService: ApiService
    ) {
        self.apiService = apiService
        super.init(output: output)
    }
}
