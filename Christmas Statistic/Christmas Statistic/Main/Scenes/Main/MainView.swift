
import UIKit

// MARK: - MainViewInterface

public protocol MainViewInterface: BaseViewInterface {
    func reloadData()
}

// MARK: - MainView

class MainView: BaseView<MainViewModelInterface> {
    private lazy var searchBar = UISearchBar(frame: .zero).then {
        $0.delegate = self
    }
    
    private lazy var collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewLayout()
    ).then {
        $0.dataSource = self
        $0.delegate = self
        $0.bounces = false
        $0.keyboardDismissMode = .interactive
        $0.backgroundColor = .clear
    }
    
    // MARK: - Setup

    override func setup() {
        collectionView.setCollectionViewLayout(
            getCollectionLayout(),
            animated: false,
            completion: nil
        )
        collectionView.register(class: MainCollectionViewCell.self)
        startKeyboardObserving()
    }

    override func setupUI() {
        addSubview(self.searchBar)
        addSubview(self.collectionView)
    }

    override func setupConstraints() {
        self.searchBar.pin([
            .super(.left),
            .super(.right),
            .super(.top),
            .other(.bottom, self.collectionView, .top)
        ])
        self.collectionView.pin([
            .super(.left), .super(.right), .super(.bottom)
        ])
    }

    // MARK: -
    
    func getCollectionLayout() -> UICollectionViewLayout {
        let itemTitleSize = NSCollectionLayoutSize(
            widthDimension: .absolute(300),
            heightDimension: .absolute(grid.space36)
        )
        let itemTitle = NSCollectionLayoutItem(layoutSize: itemTitleSize)
        itemTitle.contentInsets = .zero
        itemTitle.edgeSpacing = .init(leading: nil, top: nil, trailing: .fixed(1), bottom: .fixed(1))
        
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .absolute(grid.space48),
            heightDimension: .absolute(grid.space36)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = .zero
        item.edgeSpacing = .init(leading: nil, top: nil, trailing: .fixed(1), bottom: .fixed(1))

        let groupSize = NSCollectionLayoutSize(
            widthDimension: .absolute(300 + 27 * grid.space48),
            heightDimension: .absolute(grid.space36 + 1)
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitems: [[item], [itemTitle], [item].dublicate(count: 25)].reduce([], +)
        )
        group.contentInsets = .zero
        group.edgeSpacing = nil
        group.interItemSpacing = .none

        let section = NSCollectionLayoutSection(group: group)
        let layout = UICollectionViewCompositionalLayout(section: section)

        return layout
    }
    
    override func keyboardDidChangeState(_ state: BaseView<MainViewModelInterface>.KeyboardState) {
        if case .didShow(let frame) = state {
            collectionView.contentInset = .init(top: .zero, left: .zero, bottom: frame.height, right: .zero)
        } else {
            collectionView.contentInset = .zero
        }
    }
}

// MARK: - MainView

extension MainView: MainViewInterface {
    func reloadData() {
        self.collectionView.reloadData()
        self.collectionView.backgroundColor = .lightGray
    }
}

// MARK: - UISearchBarDelegate

extension MainView: UISearchBarDelegate {
    internal func searchBar(
        _ searchBar: UISearchBar,
        textDidChange searchText: String
    ) {
        viewModel.search(searchText: searchText)
    }

    internal func searchBarSearchButtonClicked(
        _ searchBar: UISearchBar
    ) {
        searchBar.resignFirstResponder()
    }
}

// MARK: - UITableViewDataSource

extension MainView: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.data.count
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        viewModel.data[indexPath.row].configure(collectionView: collectionView, for: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didSelect(at: indexPath)
    }
}

extension MainView: UICollectionViewDelegate {}

extension Array {
    func dublicate(count: Int) -> Array {
        (0 ..< count).compactMap { _ in self }.reduce([], +)
    }
}
