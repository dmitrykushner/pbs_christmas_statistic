
// MARK: - MainSceneAssembly

final class MainSceneAssembly: BaseSceneAssembly<
    MainViewController,
    MainView,
    MainViewModel,
    MainConfigModel
> {}
